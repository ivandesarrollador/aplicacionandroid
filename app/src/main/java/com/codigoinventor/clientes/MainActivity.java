package com.codigoinventor.clientes;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.codigoinventor.clientes.Model.ApiManager;
import com.codigoinventor.clientes.Model.BaseResponse;
import com.codigoinventor.clientes.Model.Configuracion;
import com.codigoinventor.clientes.Model.GetDataService;
import com.codigoinventor.clientes.Model.ResponseListarClientes;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private RecyclerView rvListarMisEstampas;
    private LinearLayoutManager linearLayoutManager;
    private List<ResponseListarClientes> dataSet;
    private RecyclerViewAdapter adapter;
    private FloatingActionButton btnfloating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setTitle("Clientes");
        Configuracion.editarPerfil = false;

        rvListarMisEstampas = (RecyclerView) findViewById(R.id.recyclerViewClientes);
        btnfloating = (FloatingActionButton) findViewById(R.id.btnAddClint);
        btnfloating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this,AgregarClienteActivity.class);
                startActivity(myIntent);
            }
        });
        rvListarMisEstampas.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this);
        rvListarMisEstampas.setLayoutManager(linearLayoutManager);

        listClientes();
    }

    // este esl método el cual permite listar todos los clientes que se encuentren registrados en la BD.
    private void listClientes() {
       /* progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando clientes...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();*/

        // Por medio de nuestra Interfaz podemos hacer uso de ella para poder construir nuestras llamadas a nuestro servicio Rest y ejecutarlo con l
        // la ayda de retrofit
        GetDataService service = ApiManager.createService(GetDataService.class);
        retrofit2.Call<List<ResponseListarClientes>> call = service.listClientes();
        call.enqueue(new Callback<List<ResponseListarClientes>>() {

            // método de respuesta si nuestra app se comunica correctamente por medio de peticiones http o htpps
            @Override
            public void onResponse(retrofit2.Call<List<ResponseListarClientes>> call, Response<List<ResponseListarClientes>> response) {
                makeListClientes(response.body());
                Log.w("ERRO", response.body().toString());
            }
            // método de respuesta si nuestra app no se puede comunicar por medio de peticiones http o htpps

            @Override
            public void onFailure(retrofit2.Call<List<ResponseListarClientes>> call, Throwable t) {
                makeListClientes(null);
                Log.w("ERROrrrr", t.toString());
            }
        });
    }

    // método para listar los clientes por medio de nuestro adapter
    private void makeListClientes(List<ResponseListarClientes> body) {
            dataSet = null;
            dataSet = body;
            adapter = new RecyclerViewAdapter(dataSet, this, new RecyclerViewAdapter.IOnItemClickListener() {

                // métodos para eliminar y editar a nuestros clientes implementados por medio de una interfaz en la clase RecyclerViewAdapter
                @Override
                public void editarCliente(ResponseListarClientes item) {
                    Configuracion.editarPerfil = true;
                    editCliente(item);

                }

                //método para actualizar estatus
                @Override
                public void actualizarEstatus(ResponseListarClientes item , int estatus) {
                    setClienteEstatus(item, estatus);
                }
                // método para eliminar el cliente

                @Override
                public void eliminarCliente(ResponseListarClientes item) {
                     deleteCliente(item);
                }
            });
            rvListarMisEstampas.setAdapter(adapter);
    }
 // método el cual permite actualizar el estatus del cliente
    private void setClienteEstatus(ResponseListarClientes item, int estatus) {

        GetDataService service = ApiManager.createService(GetDataService.class);
        Call<BaseResponse> call = service.setClienteEstatus(item.getIdCliente(),estatus);
        call.enqueue(new Callback<BaseResponse>() {
            // método de respuesta si la conección se puedo realiar con exito al servidor
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if(response.body().getStatus() == 1){
                    peticionExitosa(response.body().getMessage());
                }
                else{
                    peticionErronea(response.body().getMessage());
                }
            }
            //método de respuesta si la comunicacón fallo con el servidor

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                peticionErronea(t.getMessage().toString());
            }
        });
    }

    private void editCliente(ResponseListarClientes item) {
        // Pasamos datos por medio de la Activity
        Intent myIntent = new Intent(MainActivity.this,AgregarClienteActivity.class);
        myIntent.putExtra("idCliente",item.getIdCliente());
        myIntent.putExtra("nombre",item.getNombre());
        myIntent.putExtra("edad",item.getEdad());
        myIntent.putExtra("email",item.getEmail());
        myIntent.putExtra("estatus",item.getEstatus());
        int g = item.getEstatus();
        startActivity(myIntent);
    }

    // método de petición para eliminar al cliente.
    private void deleteCliente(ResponseListarClientes item) {
        GetDataService service = ApiManager.createService(GetDataService.class);
        Call<BaseResponse> call = service.deleteCliente(item.getIdCliente());
        call.enqueue(new Callback<BaseResponse>() {
            // método de respuesta si la conección se puedo realiar con exito al servidor
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if(response.body().getStatus() == 1){
                    peticionExitosa(response.body().getMessage());
                }
                else{
                    peticionErronea(response.body().getMessage());
                }
            }
            //método de respuesta si la comunicacón fallo con el servidor
            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                peticionErronea(t.getMessage().toString());
            }
        });
    }
    // método para mostrar un mensaje de error si la comunicacón fallo con el servidor
    private void peticionErronea(String message) {
        Toast.makeText(MainActivity.this,"Error:"+message,Toast.LENGTH_LONG).show();
    }
    // método generico el cual sirve para mostrar un mensaje de exito al hacer nuestra petición o algun otro proceso.
    private void peticionExitosa(String mensaje) {
        Toast.makeText(MainActivity.this,mensaje,Toast.LENGTH_LONG).show();

    }
}
