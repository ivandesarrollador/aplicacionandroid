package com.codigoinventor.clientes;

import android.os.Parcel;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telecom.Call;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.codigoinventor.clientes.Model.ApiManager;
import com.codigoinventor.clientes.Model.BaseResponse;
import com.codigoinventor.clientes.Model.Configuracion;
import com.codigoinventor.clientes.Model.GetDataService;

import java.util.regex.Pattern;

import retrofit2.Callback;
import retrofit2.Response;

public class AgregarClienteActivity extends AppCompatActivity implements View.OnClickListener {

    // atributos de la clase
    private int idCliente;
    private String nombre;
    private int edad;
    private String email;
    private  int estatus;
    private EditText editTextNombre;
    private EditText editTextEdad;
    private EditText editTextEmail;
    private RadioButton radioButtonActivo;
    private RadioButton radioButtonInactivo;
    private Button buttonRegistrar;
    private Button buttonActualizar;
    private int indicador;
    private TextInputLayout textInputLayoutNombre;
    private TextInputLayout textInputLayoutEdad;
    private TextInputLayout textInputLayoutEmail;

    // método oncreate el cual es el primero en ejecutarse
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_cliente);
        this.setTitle("Agregar cliente nuevo");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//  Referenciamos nuestros atributos con los id´s de los componentes
        editTextNombre = (EditText) findViewById(R.id.editTextNombre);
        editTextEdad = (EditText) findViewById(R.id.editTextEdad);
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        radioButtonActivo = (RadioButton) findViewById(R.id.radioActivo);
        radioButtonInactivo =(RadioButton) findViewById(R.id.radioInactivo);
        buttonRegistrar = (Button) findViewById(R.id.btnRegistrar);
        buttonActualizar = (Button) findViewById(R.id.btnActualizar);
        textInputLayoutNombre = (TextInputLayout) findViewById(R.id.textInputNombre);
        textInputLayoutEdad = (TextInputLayout) findViewById(R.id.textInputEdad);
        textInputLayoutEmail = (TextInputLayout) findViewById(R.id.textInputEmail);

      // si propiedad editarPerfil es true entonces me va a cargar los datos del cliente a actualizar en el formulario
        // el boton de registro se oculta y el otro de actualizar de pone visible
        if(Configuracion.editarPerfil == true){
            buttonRegistrar.setVisibility(View.GONE);
            buttonActualizar.setVisibility(View.VISIBLE);
            // se manda llamar al método para cargar los datos en el formulario
            cargarDatosCliente();
        }


        // se les adigna el método setOnClickListener a los componentes
        buttonRegistrar.setOnClickListener(this);
        buttonActualizar.setOnClickListener(this);


    }


// método el cual nos permite poder visualizar los datos en el formulario cuando se quieren actualizar los datos de los clientes.
    private void cargarDatosCliente() {
         idCliente = getIntent().getIntExtra("idCliente",0);
         nombre = getIntent().getStringExtra("nombre");
         edad = getIntent().getIntExtra("edad",0);
         email = getIntent().getStringExtra("email");
         estatus = getIntent().getIntExtra("estatus",0);

         editTextNombre.setText(nombre);
         editTextEmail.setText(email);
         editTextEdad.setText(String.valueOf(edad));

         if(estatus == 1){
             radioButtonActivo.setChecked(true);
             radioButtonInactivo.setChecked(false);
         }else if(estatus == 0){
             radioButtonActivo.setChecked(false);
             radioButtonInactivo.setChecked(true);
         }

    }

    // método el cual permite agregar a un nuevo cliente
    private void addCliente() {
      if(validarCampos() && validarRadiosChecked() && validarNombre()){
          if(validarEmail() && validarEdad()){
              realizarPeticion();
          }
      }
    }
    // método el cual realiza la petición para registrar a nun nuevo cliente
    private void realizarPeticion() {

         // validamos los radioButtons
        if(radioButtonActivo.isChecked()){
            indicador = 1;
        }
        else if(radioButtonInactivo.isChecked()){
            indicador = 0;
        }

      int edad = Integer.valueOf( editTextEdad.getText().toString());
        // Fragmento de código el cual nos permite hacer la llamada a nuestro servicio a consumir
        GetDataService service = ApiManager.createService(GetDataService.class);
        retrofit2.Call<BaseResponse> call = service.addCliente(editTextNombre.getText().toString(),edad,editTextEmail.getText().toString(),indicador);
        call.enqueue(new Callback<BaseResponse>() {
            // método de respuesta si la conección se puedo realiar con exito al servidor
            @Override
            public void onResponse(retrofit2.Call<BaseResponse> call, Response<BaseResponse> response) {
                if(response.body().getStatus() == 1){
                    procesarRespuesta(response.body());
                    editTextEdad.setText("");
                    editTextEmail.setText("");
                    editTextNombre.setText("");
                    radioButtonActivo.setChecked(false);
                    radioButtonInactivo.setChecked(false);

                }
                else{
                    procesarRespuesta(response.body());
                }
            }
            //método de respuesta si la comunicacón fallo con el servidor
            @Override
            public void onFailure(retrofit2.Call<BaseResponse> call, Throwable t) {
                Toast.makeText(AgregarClienteActivity.this,t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }


    // método generico el cual sirve para mostrar un mensaje de exito al hacer nuestra petición o algun otro proceso.
    private void procesarRespuesta(BaseResponse body) {
        Toast.makeText(AgregarClienteActivity.this,body.getMessage(),Toast.LENGTH_LONG).show();
    }
    // método para mostrar un mensaje de error si la comunicacón fallo con el servidor
    private void peticionErronea(String message) {
        Toast.makeText(this,"Error:"+message,Toast.LENGTH_LONG).show();
    }
    // método generico el cual sirve para mostrar un mensaje de exito al hacer nuestra petición o algun otro proceso.
    private void peticionRespuesta(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }



    // método el cual permite editar la información del cliente
    private void editCliente() {
        // Fragmento de código el cual nos permite hacer la llamada a nuestro servicio a consumir
        GetDataService service = ApiManager.createService(GetDataService.class);
        retrofit2.Call<BaseResponse> call = service.editCliente(idCliente, editTextNombre.getText().toString(), Integer.valueOf(editTextEdad.getText().toString()), editTextEmail.getText().toString(), validarRadios());
        call.enqueue(new Callback<BaseResponse>() {
            // método de respuesta si la conección se puedo realiar con exito al servidor
            @Override
            public void onResponse(retrofit2.Call<BaseResponse> call, Response<BaseResponse> response) {
                peticionRespuesta(response.body().getMessage());
                Configuracion.editarPerfil = false;
            }

            //método de respuesta si la comunicacón fallo con el servidor
            @Override
            public void onFailure(retrofit2.Call<BaseResponse> call, Throwable t) {
                peticionErronea(t.getMessage().toString());
            }
        });

    }

    //************************************************** Sección de validación de datos ****************************************

    //método para validar los campos
    private boolean validarCampos() {
        if (TextUtils.isEmpty(editTextNombre.getText()) || TextUtils.isEmpty(editTextEdad.getText()) || TextUtils.isEmpty(editTextEmail.getText())) {
            Toast.makeText(AgregarClienteActivity.this, "Falta campos por agregar", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }
    // método para validar los radioButtons para saber su estado
    private int validarRadios() {
        if(radioButtonActivo.isChecked()){
            return 1;
        }else if(radioButtonInactivo.isChecked()){
            return 0;
        }

        return 0;
    }
    private boolean validarRadiosChecked(){
        if(radioButtonActivo.isChecked() || radioButtonInactivo.isChecked()){
            return true;
        }
        else{
            Toast.makeText(AgregarClienteActivity.this, "Seleccione un estatus", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    // método para validar el nombre por medio de expreciones reglares
    private boolean validarNombre(){
        Pattern pattern = Pattern.compile("[a-zA-Z ]*+[a-zA-Z]*");
        if(pattern.matcher(editTextNombre.getText().toString()).matches() == false){
            textInputLayoutNombre.setError("Nombre invalido");
            return false;
        }else{
            textInputLayoutNombre.setError(null);
            return true;
        }
    }
    private boolean validarEdad(){
        Pattern pattern = Pattern.compile("[0-9][0-9]");
        if(pattern.matcher(editTextEdad.getText().toString()).matches() == false){
            textInputLayoutEdad.setError("Edad invalida");
            return false;
        }
        else{
            textInputLayoutEdad.setError(null);
            return true;
        }
    }
    private boolean validarEmail(){
        if(Patterns.EMAIL_ADDRESS.matcher(editTextEmail.getText().toString()).matches() == false){
           textInputLayoutEmail.setError("Correo invalido");
           return false;
        }
        else{
            textInputLayoutEmail.setError(null);
            return true;
        }
    }

    //************************************************** Fin  de validación de datos ****************************************

    @Override
    public void onClick(View view) {
   switch (view.getId()){
       case R.id.btnRegistrar:
           addCliente();
           break;
       case  R.id.btnActualizar:
           if(validarCampos()){
               if(validarEmail() && validarEdad() && validarNombre()){
                   editCliente();
               }

           }
           break;
   }

    }
}
