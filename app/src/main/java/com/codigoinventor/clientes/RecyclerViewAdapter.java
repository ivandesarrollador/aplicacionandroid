package com.codigoinventor.clientes;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.codigoinventor.clientes.Model.ResponseListarClientes;

import java.util.List;

import retrofit2.Converter;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    // implemento una interfaz con dos métodos los cuales me permitiran eliminar y editar a un cliente seleccionado
    public interface IOnItemClickListener{
        void editarCliente(ResponseListarClientes item);
        void eliminarCliente(ResponseListarClientes item);
        void actualizarEstatus(ResponseListarClientes item , int estatus);
    }
    private List<ResponseListarClientes> dataSet;
    private Context context;
    private IOnItemClickListener listener;

    // método constructor que resive 3 parametros
    public  RecyclerViewAdapter(List<ResponseListarClientes> dataSet,Context context,IOnItemClickListener listener){
        this.dataSet = dataSet;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // inflamos la vista con el item que mostraremos
       View view = LayoutInflater.from(context).inflate(R.layout.item_cliente,parent,false);
       return  new ViewHolder(view);
    }

    // sobreescribimos el método onBindViewHolder para poder accesar a cada elemnto de la lista
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
      holder.bind(dataSet.get(position),listener);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView nombre;
        private TextView email;
        private  TextView edad;
        private int estatus;
        private int id;
        private ImageButton imageButtonEdit;
        private ImageButton imageButtonDelete;
        private RadioButton radioButtonActivo;
        private RadioButton radioButtonInactivo;

        public ViewHolder(View itemView) {
            super(itemView);
            // Asignamos cada atributo con su correspondiente id para relacionarlos.
            nombre = itemView.findViewById(R.id.textViewNombre);
            email = itemView.findViewById(R.id.textViewEmail);
            edad = itemView.findViewById(R.id.textViewEdad);
            imageButtonEdit = itemView.findViewById(R.id.imageButtonEdit);
            imageButtonDelete = itemView.findViewById(R.id.imageButtonDelete);
            radioButtonActivo = itemView.findViewById(R.id.radioActivoItem);
            radioButtonInactivo = itemView.findViewById(R.id.radioInactivoItem);


        }

        public void bind(final ResponseListarClientes responseListarClientes, final IOnItemClickListener listener) {

            // asignamos cada una de sus propiedades
           nombre.setText(responseListarClientes.getNombre());
           email.setText(responseListarClientes.getEmail());
           edad.setText("edad: "+String.valueOf(responseListarClientes.getEdad()));
           id = responseListarClientes.getIdCliente();
            estatus = responseListarClientes.getEstatus();

            if (estatus == 1){
                radioButtonActivo.setChecked(true);
            }
            else if(estatus == 0){
               radioButtonInactivo.setChecked(true);
            }

           //estatus = responseListarClientes.isEstatus();
           // implementamos el método setOnClickListener con el cual podemos acceder a los metodos de la interfaz del RecyclerViewAdapter
           imageButtonEdit.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   // método para editar
                   listener.editarCliente(responseListarClientes);
               }
           });

           imageButtonDelete.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   // método para eliminar
                   listener.eliminarCliente(responseListarClientes);
                   // Removemos los itemas de la lista despues de eliminarlo de la db
                   dataSet.remove(getAdapterPosition());
                   // notificamos al recycler del elemento que se elimino
                   notifyItemRemoved(getAdapterPosition());
                   notifyItemRangeChanged(getAdapterPosition(),dataSet.size());
               }
           });
           radioButtonActivo.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {

                   // validamos los rediobuttons
                   int estatuss = 0;
                   if (radioButtonActivo.isChecked()){
                       estatuss =1;
                   }
                   else if(radioButtonInactivo.isChecked()){
                       estatuss = 0;
                   }
                   listener.actualizarEstatus(responseListarClientes,estatuss);
               }
           });
            radioButtonInactivo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // validamos los rediobuttons
                    int estatuss = 0;
                    if (radioButtonActivo.isChecked()){
                        estatuss =1;
                    }
                    else if(radioButtonInactivo.isChecked()){
                        estatuss = 0;
                    }
                    listener.actualizarEstatus(responseListarClientes,estatuss);
                }
            });



        }
    }
}
