package com.codigoinventor.clientes.Model;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitInstance {

    // Clase de Retrofit la cual nos va a permitir construir nuestras urls y convertir nuestro json en objetos java
    private static Retrofit retrofit;

    public  static Retrofit getRetrofitInstance(){
        if (retrofit == null)
        {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Configuracion.URL_BASE)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
