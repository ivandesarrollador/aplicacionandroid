package com.codigoinventor.clientes.Model;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

// interfaz la cual nos permite construir la estructura de nuestros servicios a utilizar

public interface GetDataService {

    @FormUrlEncoded
    @POST(Configuracion.AGREGAR_CLIENTE)
    Call<BaseResponse>addCliente(@Field(Configuracion.NOMBRE) String nombre,
                                                 @Field(Configuracion.EDAD) int edad,
                                                 @Field(Configuracion.EMAIL) String email,
                                                 @Field(Configuracion.ESTATUS) int estatus);

    // firma de método para listar clientes
    @GET(Configuracion.LISTAR_CLIENTES)
    Call<List<ResponseListarClientes>>listClientes();
    // firma de método de editar la información de los clientes
    @FormUrlEncoded
    @POST(Configuracion.EDITAR_CLIENTE)
    Call<BaseResponse>editCliente(@Field(Configuracion.ID_CLIENTE) int id,
                                  @Field(Configuracion.NOMBRE) String nombre,
                                  @Field(Configuracion.EDAD) int edad,
                                  @Field(Configuracion.EMAIL) String email,
                                  @Field(Configuracion.ESTATUS) int estatus);

    // Firma de método eliminar clientes
    @FormUrlEncoded
    @POST(Configuracion.ELIMINAR_CLIENTE)
    Call<BaseResponse>deleteCliente(@Field(Configuracion.ID_CLIENTE) int id);

   // Firma de método actualizar estatus
    @FormUrlEncoded
    @POST(Configuracion.ACTUALIZAR_ESTATUS)
    Call<BaseResponse>setClienteEstatus(@Field(Configuracion.ID_CLIENTE) int id,
                                        @Field(Configuracion.ESTATUS) int estatus);

}
