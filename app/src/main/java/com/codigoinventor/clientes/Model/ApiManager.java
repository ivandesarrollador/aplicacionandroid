package com.codigoinventor.clientes.Model;

import android.content.res.Configuration;

import com.codigoinventor.clientes.BuildConfig;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiManager {
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder().connectTimeout(Configuracion.TIME_OUT, Configuracion.TIME_UNIT)
            .readTimeout(Configuracion.TIME_OUT, Configuracion.TIME_UNIT)
            .writeTimeout(Configuracion.TIME_OUT, Configuracion.TIME_UNIT)
            .addInterceptor(getLogs());
    private static Retrofit.Builder builder = new Retrofit.Builder().baseUrl(Configuracion.URL_BASE).addConverterFactory(GsonConverterFactory.create());

    private static HttpLoggingInterceptor getLogs() {
        if (BuildConfig.DEBUG) {
            return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE);
        }
    }

    public static <S> S createService(Class<S> serviceClass) {
        return builder.client(httpClient.build()).build().create(serviceClass);
    }
}
