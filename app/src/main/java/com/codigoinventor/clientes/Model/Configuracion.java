package com.codigoinventor.clientes.Model;

import java.util.concurrent.TimeUnit;

public class Configuracion {

    //Time out
    public static final int TIME_OUT = 60;
    public static final TimeUnit TIME_UNIT = TimeUnit.SECONDS;

  // constantes las cuales ocupo para poder llamar a cada uno de los métodos  con su base url
    public static  final String URL_BASE = "http://192.168.91.1/examen/public/index.php/api/";
    public  static  final String AGREGAR_CLIENTE ="addCliente";
    public  static  final String LISTAR_CLIENTES ="listClientes";
    public  static  final String EDITAR_CLIENTE ="editCliente";
    public  static  final String ELIMINAR_CLIENTE ="deleteCliente";
    public  static  final String ACTUALIZAR_ESTATUS ="setClienteEstatus";

    // constantes las cuales envio como parametros al servicio rest
    public static final String ID_CLIENTE = "idCliente";
    public static final String NOMBRE = "nombre";
    public static final String EDAD = "edad";
    public static final String EMAIL = "email";
    public static final String ESTATUS = "estatus";
    public static boolean editarPerfil = false;


}
