package com.codigoinventor.clientes.Model;

public class ResponseListarClientes {

    // clase responseListarClientes la cual me genera los objetos correspondientes los cuales puedo manipular sus metodos y atributos
    private int idCliente;
    private String nombre;
    private int edad;
    private String email;

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    private int estatus;


    public void setStatus(int status) {
        this.status = status;
    }

    private int status;

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


}

